<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.amnesty
 *
 * @author      Tom Kattner
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

JLoader::import('joomla.version');

$app = Factory::getApplication();
$document = Factory::getDocument();
$user = Factory::getUser();
$lang = Factory::getLanguage();

// Output as HTML5
$this->setHtml5(true);

// Output as HTML5
$this->setHtml5(true);

// Set Time Output to current Language
$locale = $lang->getLocale();
setlocale(LC_TIME, $locale[0]);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

// Enable assets
$webAssetManager = $this->getWebAssetManager();
$webAssetManager->useStyle('template.amnesty');
$webAssetManager->registerAndUseStyle('main', 'media/templates/site/amnesty/css/template.css');
$webAssetManager->registerAndUseScript('main', 'media/templates/site/amnesty/js/template.js');

// Background Image
if ($this->params->get('bgImage')) {
    $bgImageURI = JRoute::_($this->baseurl . '/' . $this->params->get('bgImage'));
    $this->addStyleDeclaration('
	body {
		background-image: url(' . $bgImageURI . ');
	}');
}

// Social Media Links
$socialMedia = array();
if ($this->params->get('linkFacebook')) {
    $socialMedia['Facebook'] = 'https://www.facebook.com/' . $this->params->get('linkFacebook') . '/';
}

if ($this->params->get('linkTwitter')) {
    $socialMedia['Twitter'] = 'https://twitter.com/' . $this->params->get('linkTwitter') . '/';
}

if ($this->params->get('linkInstagram')) {
    $socialMedia['Instagram'] = 'https://www.instagram.com/' . $this->params->get('linkInstagram') . '/';
}

if ($this->params->get('linkYouTube')) {
    $socialMedia['YouTube'] = 'https://www.youtube.com/channel/' . $this->params->get('linkYouTube') . '/';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $document->language; ?>" lang="<?php echo $document->language; ?>">
<head>
    <jdoc:include type="metas"/>
    <jdoc:include type="styles"/>
    <jdoc:include type="scripts"/>
</head>
<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
echo($document->direction == 'rtl' ? ' rtl' : '');
?>">
<div id="page">
    <div id="banner">
        <div id="logo"><a href="/" title="zur Startseite">&nbsp;</a></div>
        <div id="donate">
            <a href="https://www.amnesty.de/spendentool/1367" target="_blank">online spenden</a>
            <br/>
            <a href="https://www.amnesty.de/foerdererwerden/1367"
               target="_blank"
               title="Als Förderer*in unterstützen Sie die Menschenrechtsarbeit von Amnesty International mit einem Jahresbeitrag von mindestens 60,- €. Förder*innen sind keine Mitglieder, erhalten aber die Zeitschrift Amnesty Journal kostenlos und können sich bei Interesse an einzelnen Aktionen beteiligen. Sie erhalten für die gesamte Fördersumme jährlich eine Zuwendungsbestätigung zur Vorlage beim Finanzamt.">dauerhaft f&ouml;rdern</a>
        </div>
        <?php if (!empty($socialMedia)) : ?>
            <div id="social">
                <?php foreach ($socialMedia as $title => $link) : ?>
                    <a href="<?php echo $link; ?>"
                       target="_blank"
                       class="fab fa-<?php echo strtolower($title); ?>"
                       title="<?php echo $title; ?>"></a>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div id="navigation-main">
        <div class="nav-toggle">
            <span class="icon-menu-3"></span>
        </div>
        <jdoc:include type="modules" name="position-1"/>
    </div>
    <div id="messages">
        <jdoc:include type="message"/>
    </div>
    <div id="container">
        <div id="content">
            <jdoc:include type="component"/>
        </div>
        <div id="sidebar-right" class="sidebar">
            <div class="sidebar-toggle">
                <span class="icon-arrow-left">&nbsp;</span>
            </div>
            <div class="sidebar-content">
                <!-- Begin Right Sidebar -->
                <jdoc:include type="modules" name="position-7" style="xhtml"/>
                <!-- End Right Sidebar -->
            </div>
        </div>
    </div>
    <?php if ($this->countModules('footer')) : ?>
    <footer id="footer" aria-label="Footer">
        <jdoc:include type="modules" name="footer"/>
    </footer>
    <?php endif; ?>
</div>
</body>
</html>
