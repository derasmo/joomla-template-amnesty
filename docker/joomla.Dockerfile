FROM joomla:4-php7.4-apache

RUN userdel www-data
ARG APACHE_UID
RUN useradd -l -u ${APACHE_UID} www-data
RUN mkdir -p /home/www-data; \
    chown -R ${APACHE_UID} /home/www-data; \
    chown -R ${APACHE_UID} /var/www/html

# Install Node.js
RUN apt-get update; \
    apt-get install -y gnupg libpng-dev curl; \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs;

RUN npm install -g sass

RUN cd /etc/apache2/mods-enabled; \
    ln -s ../mods-available/expires.load expires.load; \
    ln -s ../mods-available/authz_groupfile.load authz_groupfile.load; \
    ln -s ../mods-available/headers.load headers.load

RUN apt install -y git curl wget

RUN composer global require joomlatools/console --no-progress; \
    chmod +x /root/.composer/vendor/bin/joomla; \
    ln -s /root/.composer/vendor/bin/joomla /usr/local/bin/joomla

# Configuration for Mailhog
RUN apt-get update \
    && apt-get install -y msmtp msmtp-mta mailutils \
    && apt-get clean \
    && echo "sendmail_path = /usr/sbin/sendmail -S mailhog:1025" > /usr/local/etc/php/conf.d/mailhog.ini \
    && echo "port 1025" >> /etc/msmtprc \
    && echo "host mailhog" >> /etc/msmtprc \
    && echo "from mailhog@example.com" >> /etc/msmtprc
