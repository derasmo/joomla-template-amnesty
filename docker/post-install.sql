USE joomla;

SET @db_prefix = 'jos';

SET @template_manifest_cache = '{"name":"amnesty","type":"template","creationDate":"2016-04-09","author":"Tom Kattner","copyright":"Tom Kattner 2012","authorEmail":"administrator@ai-campus.de","authorUrl":"http:\/\/www.ai-campus.de","version":"0.1.0","description":"TPL_AMNESTY_XML_DESCRIPTION","group":"","filename":"templateDetails"}';
SET @template_field_names = "`name`,`type`,`element`,`folder`,`client_id`,`enabled`,`manifest_cache`,`params`,`custom_data`";
SET @template_field_values = CONCAT("'amnesty','template','amnesty','','0','1','", @template_manifest_cache, "','{}',''");
SET @add_template = CONCAT("INSERT INTO ", @db_prefix, "_extensions (", @template_field_names, ") VALUES (", @template_field_values, ")");

SET @style_params = '{"bgImage":"","linkFacebook":"AmnestyDeutschland","linkTwitter":"amnesty_de","linkInstagram":"amnestydeutschland"}';
SET @style_field_names = "`template`,`client_id`,`home`,`title`,`params`";
SET @style_field_values = CONCAT("'amnesty','0','1','amnesty - Default','", @style_params, "'");
SET @add_style = CONCAT("INSERT INTO ", @db_prefix, "_template_styles (", @style_field_names, ") VALUES (", @style_field_values, ")");

SET @disable_active_style = CONCAT("UPDATE ", @db_prefix, "_template_styles SET `home` = 0 WHERE `client_id` = 0");

/* prepare all provided queries */
PREPARE stmt1 FROM @add_template;
PREPARE stmt2 FROM @disable_active_style;
PREPARE stmt3 FROM @add_style;

/* execute all provided queries */
EXECUTE stmt1;
EXECUTE stmt2;
EXECUTE stmt3;
