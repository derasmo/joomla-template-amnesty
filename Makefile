TEMPLATE_NAME="amnesty"
JOOMLA_ROOT="public/httpdocs"

help:
	@echo "Usage: make [command]\n"
	@echo "possible commands:"
	@cat Makefile | grep "##" | grep -v "Makefile" | sed -E "s|(.*):.*##(.*)|\t\1:=\2|" | column -t -s "="

init: ## init local docker .env
	mkdir -p dcc-data
	mkdir -p ${JOOMLA_ROOT}
	cp docker/.env.example docker/.env
	printf "USER_ID=%s" $$(id -u) >> docker/.env

start: init ## start local working container
	cd docker && docker-compose up -d

stop: ## stop local working container
	cd docker && docker-compose down

status: ## list running containers
	cd docker && docker-compose ps

bash: ## jump into working container
	cd docker && docker-compose exec --user=www-data joomla bash

bash-root: ## jump into working container as root
	cd docker && docker-compose exec joomla bash

build: init ## build local working container
	cd docker && docker-compose build --no-cache

fix-permissions: ## fix permissions for www-data in joomla container
	cd docker && docker-compose exec joomla /bin/chown www-data -R /var/www/html

post-install: sass-build ## post install (after installation wizard has been run) modifications in database
	cd docker && docker-compose exec --user=www-data joomla bash -c "ln -s /project/src /var/www/html/templates/${TEMPLATE_NAME}"
	cd docker && docker-compose exec --user=www-data joomla bash -c "ln -s /project/src/media /var/www/html/media/templates/site/${TEMPLATE_NAME}"
	cd docker && docker-compose exec mysql /usr/bin/mysql -uroot -proot -e "$$(cat post-install.sql)"

sass-build:
	cd docker && docker-compose exec --user=www-data joomla /usr/bin/sass /project/scss:/project/src/media/css

sass-watch:
	cd docker && docker-compose exec --user=www-data joomla /usr/bin/sass --watch /project/scss:/project/src/media/css
