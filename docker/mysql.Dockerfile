FROM dnhsoft/mysql-utf8:5.7

RUN userdel mysql
ARG MYSQL_UID
RUN useradd -l -u ${MYSQL_UID} mysql
