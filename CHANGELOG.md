# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.2.0] - 2024-06-25
### Added
- styles for pagination
- YouTube can be added in the social media section

### Changed
- loading of scripts and styles through Joomla! 4 asset management
- font awesome is now used from joomla core library
- text for financial support has been changed

### Removed
- icomoon support has been removed
- slick slider has been removed

## [0.1.0] - 2020-06-07
### Added
- started a CHANGELOG
- initialised project in current state
- leading articles in category blog will be displayed with slick slider
- social media links can be added to the header

[0.1.0]: https://gitlab.com/derasmo/joomla-template-amnesty/-/tags/v0.1.0
[0.2.0]: https://gitlab.com/derasmo/joomla-template-amnesty/-/tags/v0.2.0
