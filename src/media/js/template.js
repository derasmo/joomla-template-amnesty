jQuery(function($) {
	$(window).scroll(function(){
		if($(this).scrollTop() > $('#banner').height() + parseInt($('body').first().css('padding-top')))
		{
			if(!$('#navigation-main').hasClass('fixednav'))
			{
				var containerPadding = parseInt($('#container').css('padding-top')) + $('#navigation-main').height();

				$('#navigation-main').width($('#page').width());
				$('#navigation-main').css('border-bottom', '1px solid yellow');
				$('#navigation-main').addClass('fixednav');
				$('#container').css('padding-top', containerPadding);
			}
		}
		else
		{
			if($('#navigation-main').hasClass('fixednav'))
			{
				$('#navigation-main').removeAttr('style');
				$('#navigation-main').removeClass('fixednav');
				$('#container').removeAttr('style');
			}
		}
	});

	$('.mainlevelmainnav').mouseover(function() {
		$(this)
			.parent()
			.find('ul')
			.css({top: $(this).position().top + $(this).outerHeight(), left: $(this).position().left - 1});
	});

	$('.sidebar-toggle').click(function() {
		$(this)
			.parent('.sidebar')
			.toggleClass('active');
	});
	
	/*
	 * workspace
	 * 
	 */
	
	$('.nav-toggle').click(function() {
		$('#navigation-main ul.nav').toggleClass('toggle-active');
	});
});