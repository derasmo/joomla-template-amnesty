# Joomla Template Amnesty

An Amnesty themed Joomla! template.

## setup local development

* run `make start` to initialise and run docker environment
* visit `localhost` and run through installation wizard
    * DB Host: mysql
    * DB Name: joomla
    * DB User Name: root
    * DB User Password: root
* run `make post-install` to register template extension and link the source folder
